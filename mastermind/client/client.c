/**
* @file client.c
* @author Betim Bryma <e1325255@student.tuwien.ac.at>
* @date 11.01.2015
*
* @brief Client that tries to solve the mastermind game, by guessing
*
**/
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
static char *pgm_name = "client";
 
 
/**
* @brief terminate program on program error
* @param msg format string
*/
static void error(char *msg);
 
/**
* @brief it is going to give us a socket
* @param host the host name
* @param port the port number, as a String
* @return pointer to the socket
*/
static int open_socket(char *host, char *port);
 
/**
* @brief Read message from socket
*
* This code *illustrates* one way to deal with partial reads
*
* @param fd Socket to read from
* @param buffer Buffer where read data is stored
* @param n Size to read
* @return Pointer to buffer on success, otherwise NULL
*/
static uint8_t *read_from_server(int fd, uint8_t *buffer, size_t n);
 

 
static void error(char *msg) {
        fprintf(stderr, "For programm %s, %s: %s\n",pgm_name, msg, strerror(errno));
        exit(1);
}
 
 
static int open_socket(char *host, char *port) {
        struct addrinfo *res;
        struct addrinfo hints;
        memset(&hints, 0, sizeof(hints));
        hints.ai_family = AF_INET;
        hints.ai_socktype = SOCK_STREAM;
        if (getaddrinfo(host, port, &hints, &res) == -1)
                error("Can't resolve the address");
        int d_sock = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
        if (d_sock == -1)
                error("Can't open socket");
        int c = connect(d_sock, res->ai_addr, res->ai_addrlen);
        freeaddrinfo(res);
        if (c == -1)
                error("Can't connect to socket");
 
        return d_sock;
}
 
 
 
static uint8_t *read_from_server(int fd, uint8_t *buffer, size_t n){
        size_t bytes_recv = 0;
        do {
                ssize_t r;
                r = recv(fd, buffer + bytes_recv, n - bytes_recv, 0);
                if (r <= 0) {
                        return NULL;
                }
                bytes_recv += r;
        } while (bytes_recv < n);
 
        if (bytes_recv < n) {
                return NULL;
        }
        return buffer;
}
 
 
/**
* @brief Program entry point, the main function
* @param argc The argument counter
* @param argv The argument vector
* @return 0 on success, and a bigger than 1 in case of an Error
* 2 in case of parity, 3 in case of a lost game, and 4 if it's both a lost game, and parity fault
*/
int main(int argc, char *argv[]) {
        if (argc != 3){
                fprintf(stderr,"Invalid number of arguments\n");
                return 1;}
 
        //Checking if the second argument is a valid number
        char *endptr;
        int port = strtol(argv[2], &endptr, 10);
        if (*endptr != '\0'){
                fprintf(stderr, "Garbage at the end of string");
                return 1;
                }
        if (errno != 0)
                error("Something went wrong");
        if ((port == -1) || (port <= 1024)){
                fprintf(stderr,"Invalid port number\n");
                return 1;}
        int rund = 0;
        int d_sock;
        d_sock = open_socket(argv[1], argv[2]);
        printf("%s", "Conneection successful!\n");
        while (1) {
                rund++;
                int i;
                uint16_t msg = 0;
                uint8_t parity = 0;
                uint8_t colors[] = { 0,1,2,3,4,5,6,7 };
                for (i = 0; i<5; ++i) {
                        uint8_t colorVal = colors[rand() % 8] & 0x7;
                        msg |= (colorVal << (i * 3));
                }
 
                //parity calculation
                for (i = 0; i<16; i++) {
                        parity ^= (msg >> i);
                }
               
 
                msg |= ((parity & 0x1) << 15);
                if (send(d_sock, &msg, 2, 0) == -1)
                        error("Something happened");
 
                uint8_t buffer;
                if (read_from_server(d_sock, &buffer, 1) == NULL)
                        error("something happened!");
 
 
                int red = (buffer & 0x7);
               // int white = ((buffer >> 3) & 0x7);
 
 
                if (red == 5) {
                        fprintf(stdout, "Congratulations, you won the game. You needed %d Rounds, but you still made it :) \n", rund);
                        return 0;
                }
 
 
                if (((buffer >> 6) & 0x1) == 1) {
                        if ((buffer >> 7) == 1) {
                                fprintf(stderr, "You lost the game, plus there was a parity error. Round: %d\n", rund);
                                return 4;
                        }
 
                        fprintf(stderr, "Sorry, there was a parity error. Better luck next time. Round: %d\n", rund);
                        return 2;
                }
 
                if ((buffer >> 7) == 1) {
                        fprintf(stderr, "You lost the game after %d rounds, better luck next time :) \n", rund);
                        return 3;
                }
 
 
        }
        return 0;

    }