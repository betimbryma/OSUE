/**
 * @file hangman-server.c
 * @author Betim Bryma <e1325255@student.tuwien.ac.at>
 * @date 05.01.2016
 * @brief This module behaves like the server that allows clients to interact and play hangman
 * @details A client that wants to play hangman has to 'connect' with this server. All communications are done via SHARED MEMORY
*/

#include <stdio.h>
#include <unistd.h>
#include <semaphore.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include <semaphore.h>
#include <stdlib.h>
#include <signal.h>
#include <ctype.h>
#include <errno.h>

#define SEM_1 "/semaphore_1"
#define SEM_2 "/semaphore_2"
#define SEM_3 "/semaphore_3"
#define SHM_NAME "/Betimemory"
#define PERMISSION (0600)
#define MAX_WORD_LENGTH (255)
#define ERRORS_MAX (9)

/** @brief The name of the program*/
 static const char *progname = "hangman-server";

/** @brief a struct where the players are saved (linked-list)*/
 static struct Player *players = NULL;

/** @brief the game struct where the server communicates with its clients*/
 static struct Game *game = NULL;

/** @brief the list of words that have been inserted so far whether via a file, or stdin*/
 static struct Words *words = NULL;

/** @brief semaphore that tells the server when it may proceed and  enter the critical area*/
 static sem_t *s1;

/** @brief semaphore that tells the client when it may proceed*/
 static sem_t *s2;

/** @brief semaphore that tells the client when it may proceed*/
 static sem_t *s3;

/** @brief the ID that starts by 0 but increments everytime a new client registers*/
 static int id = 0;

/** @brief an int variable to know whether the first semaphore has been opened*/
 static int sem1 = 0;

 /** @brief an int variable to know whether the second semaphore has been opened*/
 static int sem2 = 0;

 /** @brief an int variable to know whether the second semaphore has been opened*/
 static int sem3 = 0;


/**
 * @brief create1 is goign to create a new 'Player' for the server that is going to be saved on the linked list
 * @details it does that with by using the current ID, because each client has to have a unique ID
 * @param id the ID of the client
 * @return the new pointer to the struct Player that has been created
*/
 static struct Player* create1(int id);

/**
 * @brief free_resources is going to release the allocated memory 
 * @details free_resources is called when the server is termintaing, it 
 * frees the memory that has been allocated (Words, Player, Game), semaphores and so on
*/
 static void free_resources(void);

/**
 * @brief if an error occurs, then we want to shutdown this server
 * @details it is going to use the progname variable (the name of the program)
 * @param exitcode is the exit value of the program
 * @param message eventual informations about the error that occured
*/
 static void bail_out(int exitcode, char *message);

/**
 * @brief the handler method for SIGINT and SIGTERM
 * @details because the program cannot terminate without freeing the memory and taking care of semaphores
 * @param sig the signal
*/
 static void terminating(int sig);

/**
 * @brief the function that is going to catch our signals
 * @param sig, represents the signal
 * @param the second parameter represents a pointer to the handler function
 * @return returns 0 on success; on error, -1 is returned
*/
 static int catch_signal(int sig, void (*handler)(int));

/**
 * @brief checkAscii is going to check whether a specific character is ASCII character or not
 * @details in our case only letters and the 'space' is allowes, everything else is considered as false
 * @param the character that we want to check for
 * @return 0 if it does not fulfill the conditions, otherwise 1
*/
 static int checkAscii(char c);

/**
 * @brief the function create is going to create a new pointer to a struct Words with the text inside
 * @param text that is going to represent the word
 * @return a pointer to the new word
*/
 static struct Words* create(char *text);

/**
 * @brief checking to see whether the client guessed right or not
 * @details by checking the guess character in the game struct that is shared, the function determines
 * whether the client guessed or not, and if he may have won the game or lost it
*/
 static void guess(void);

/**
 * @brief register the new player in the game
 * @details it checks if there are words available for the current user (game->id) and if it does
 * then the client can go on playing
*/
 static void play(void);

/**
 * @brief if a client terminated, then we want to free the memory that was used by the client
*/
 static void clientTerminated(void);

/**
 * @brief enumration used for 'Flags' in the process of communication between the client and the server
*/
 typedef enum
 {
 NEWGAME, /** the client onts to start a new game, as a new client*/
 REPLAY, /** the client wants to play another game*/
 TERMINATED, /** the client has been terminated*/
 PLAY, /** the client is playin a current game*/
 }request;

/**
 * @brief the player struct that is going to be used for saving the clients
*/
 struct Player{
	int id; /** the ID of the client*/
	int alive; /** whether the client is alive or not*/
	int games; /** how many games has the client played so far*/
	int wins; /** how many times has the client won so far*/
	int losts; /** how many times has the client lost so far*/
	int mistakes; /** how many mistakes has the client made in this round*/
	struct Words *word; /** the secred words that the client has to guess*/
	char secret[MAX_WORD_LENGTH]; /** the current word that the client is trying to guess*/
	char response[MAX_WORD_LENGTH]; /** the response that the client gets in return*/
	struct Player *next; /** a pointer to the next player*/
 };

/**
 * @brief the game struct that is going to be used for communication between the client and the server via shared memory
*/
 struct Game{
	int id; /** the ID of the client*/
	int error; /** whether something went wrong */
	char guess; /** the character that the client guessed*/
	int server_alive; /** whether the server is alive (1) or not (0)*/
	int won; /** whether the client won the game (1) or not (0)*/
 	int lost; /** whether the client lost the game (1) or not (0)*/
	request flag; /** the flag that is used for commucation, to distinct cases*/
	int mistakes; /** how many mistakes has the client made so far*/
	int wins; /** how many times has the client won so far*/
	int losts; /** how many times has the client lost so far */
	char response[MAX_WORD_LENGTH]; /** the responses from the server */
	int games; /** how many games have been played so far*/
 	
 };

/**
 * @brief a struct that it going to be used to save the words that we have in our dictationary
*/
 struct Words{
 	char *word;
 	struct Words *next;
 };

 static struct Player* create1(int id){
 	struct Player *i = malloc(sizeof(struct Player));
 	i->id=id;
 	i->alive=1;
 	i->wins=0;
 	i->losts=0;
 	i->mistakes=0;
 	i->word=words;
 	i->next = NULL;
 	return i;
 }


 static void bail_out(int exitcode, char *message){
 	(void) fprintf(stderr,"%s: ", progname);
 	(void) fprintf(stderr,"%s",message);
 	if(errno!=0)
 		(void) fprintf(stderr,": %s",strerror(errno));
 	(void) fprintf(stderr,"\n");
 	free_resources();
 	exit(exitcode);
 }

 static void free_resources(void){


 	game->server_alive = 0;
 	strncpy(game->response, "The server has been shut down",MAX_WORD_LENGTH);
 	sem_post(s3);


 	struct Words *currentWord = words;
 	struct Words *tempWord = currentWord;
 	while(currentWord!=NULL){
 		free(currentWord->word);
 		currentWord=currentWord->next;
 		free(tempWord);
 		tempWord=currentWord;
 	}

 	struct Player *currentPlayer = players;
 	struct Player *tempPlayer = currentPlayer;
 	while(currentPlayer!=NULL){
 		currentPlayer=currentPlayer->next;
 		free(tempPlayer);
 		tempPlayer=currentPlayer;
 	}

 	if((munmap(game,sizeof *game)) == -1)
 		(void) fprintf(stderr, "%s\n", "munmap failed");
 	if(shm_unlink(SHM_NAME) == -1)
 		(void) fprintf(stderr, "%s\n", "shm_unlink failed");
 	if(sem1 == 1){	
 		if((sem_close(s1)) == -1)
 			(void) fprintf(stderr, "%s\n", "sem_close failed");
 	}

 	if(sem2 == 1){
 		if((sem_close(s2)) == -1)
 			(void) fprintf(stderr, "%s\n", "sem_close failed");
 	}

 	if(sem3 == 1){
 		if((sem_close(s3)) == -1)
 			(void) fprintf(stderr, "%s\n", "sem_close failed");
 	}
 	if((sem_unlink(SEM_1)) == -1)
 		(void) fprintf(stderr, "%s\n", "sem_unlink failed");
 	if((sem_unlink(SEM_2)) == -1)
 		(void) fprintf(stderr, "%s\n", "sem_unlink failed");
 	if((sem_unlink(SEM_3)) == -1)
 		(void) fprintf(stderr, "%s\n", "sem_unlink failed");

 }


 static void terminating(int sig){
 	(void) printf("\nThe server is now being shut down\n");
 	free_resources();
 	exit(1);
 }

 static int catch_signal(int sig, void (*handler)(int)){
 	struct sigaction action;
 	action.sa_handler = handler;
 	sigemptyset(&action.sa_mask);
 	action.sa_flags = 0;
 	return sigaction (sig, &action, NULL);
 }

 static int checkAscii(char c){
 	if(c==32 || (c >= 65 && c <= 90) || (c >= 97 && c <= 122) || c == '\0')
 		return 1;
 	else
 		return 0;
 }

 static struct Words* create(char *text){
 	struct Words *i = malloc(sizeof(struct Words));
 	i->word=strdup(text);
 	i->next=NULL;
 	return i;
 }

 static void guess(void){
 	struct Player *current = players;
 	while(current!=NULL){
 		if(current->id==game->id)
 			break;

 		current=current->next;
 	}

 	if(current == NULL){
 		strncpy(game->response,"Player not found. Please register in the server.\n",MAX_WORD_LENGTH);
 		game->error=1;
 		return;
 	}

 	int guessed = 0;
 	int check = 1;
 	for(int i =0; i<strlen(current->secret); i++){
 		if(toupper(current->secret[i])== toupper(game->guess) && game->guess != ' '){
 			current->response[i]=toupper(game->guess);
 			guessed = 1;
 		}
 	}
 	if(guessed == 0){
 		current->mistakes=current->mistakes + 1;
 		game->mistakes=current->mistakes;
 	}


 	(void) strncpy(game->response,current->response,MAX_WORD_LENGTH);

 	for(int i =0; i<strlen(current->secret); i++){
 		if(toupper(current->secret[i]) != toupper(current->response[i])){

 			check = 0;
 			break;
 		}
 	}

 	if(check == 1){
 		game->won = 1;
 		current->wins = current->wins + 1;
 		game->wins=game->wins+1;
 	}

 	if(current->mistakes == ERRORS_MAX){
 		game->lost = 1;
 		current->losts = current->losts + 1;
 		game->losts=game->losts+1;
 	}

 }

 static void play(void){
 	struct Player *current = players;
 	while(current != NULL){
 		if(current->id == game->id){
 			if(current->word == NULL || ((game->flag==REPLAY )&& (current->word->next == NULL))){
 				sprintf(game->response,"You have used all the words already. We hope that you had fun\nLosts: %i Wins: %i \n",current->losts,current->wins);
 				game->error=1;
 				game->wins=current->wins;
 				game->losts=current->losts;
 				(void) clientTerminated();
 				return;
 			}

 			if(game->flag==REPLAY)
 				current->word=current->word->next;
 			strncpy(current->secret,current->word->word,MAX_WORD_LENGTH);
 			strncpy(current->response,current->secret,MAX_WORD_LENGTH);
 			for(int i=0;i<strlen(current->secret);i++){
 				if(current->secret[i] == ' ')
 					current->response[i] = ' ';
 				else
 					current->response[i] = '_';
 			}
 			strncpy(game->response,current->response,MAX_WORD_LENGTH);
 			game->error=0;
 			return;
 		}
 		current=current->next;
 	}


 	strncpy(game->response,"Player not found. Please be patient until the server runs first\n",MAX_WORD_LENGTH);
 	game->error=2;
 }

 static void clientTerminated(void){
 	struct Player *temp=players;
 	struct Player *temp1=temp;
 	(void) printf("Client with id %i is now disconnected.\n",game->id);
 	while(temp!=NULL){
 		if(temp->id==game->id){
 			if(temp==players){
 				players=players->next;
 			}
 			else{
 				temp1->next=temp->next;
 			}
 			free(temp);
 			break;
 		}
 		temp1=temp;
 		temp=temp->next;
 	}
 }


 int main(int argc, char **argv){

 	if(catch_signal(SIGINT, terminating) == -1)
 		bail_out(EXIT_FAILURE,"Couldn't map the handler for SIGINT");
 	if(catch_signal(SIGTERM, terminating) == -1)
 		bail_out(EXIT_FAILURE,"Couldn't map the handler for SIGTERM");

 	int shmfd = shm_open(SHM_NAME, O_RDWR | O_CREAT, PERMISSION);

 	if(shmfd == -1)
 		bail_out(EXIT_FAILURE, "shm_open failed");

 	if(ftruncate(shmfd, sizeof *game) == -1)
 		bail_out(EXIT_FAILURE, "ftruncate failed");

 	game = mmap(NULL, sizeof *game, PROT_READ | PROT_WRITE, MAP_SHARED, shmfd, 0);



 	if(game == MAP_FAILED)
 		bail_out(EXIT_FAILURE, "mmap failed");

 	if(close(shmfd) == -1)
 		bail_out(EXIT_FAILURE, "close failed");
 	game->server_alive=1;

 	sem_unlink(SEM_1);
 	sem_unlink(SEM_2);
 	sem_unlink(SEM_3);


 	s1 = sem_open(SEM_1, O_CREAT | O_EXCL, PERMISSION, 0);
 	s2 = sem_open(SEM_2, O_CREAT | O_EXCL, PERMISSION, 1);
 	s3 = sem_open(SEM_3, O_CREAT | O_EXCL, PERMISSION, 0);

	/**Now checking if each semaphore has been opened in order to know which ones can we close in bail_out*/

 	if(s1 == SEM_FAILED)
 		bail_out(EXIT_FAILURE,"sem_open");
 	sem1 = 1; //keeping track, for example s1 is now open, so it can be closed if we want it to

 	if(s2 == SEM_FAILED)
 		bail_out(EXIT_FAILURE,"sem_open");
 	sem2 = 1;

 	if(s2 == SEM_FAILED)
 		bail_out(EXIT_FAILURE,"sem_open");
 	sem2 = 1;


 	if(argc > 2){
 		(void) fprintf(stderr,"USAGE: %s <input_file>",progname);
 		exit(EXIT_FAILURE);
 	}

	if(argc == 2) /*We read from a file*/
 	{

 		FILE *in = fopen(argv[1],"r");
 		if(in==NULL)
 			bail_out(EXIT_FAILURE,"fopen failed");

 		char message[MAX_WORD_LENGTH];
 		struct Words *next;
 		struct Words *i = NULL;
 		char finalmessage[MAX_WORD_LENGTH];
 		while(fgets(message,sizeof(message),in)!=NULL){

 			if(message[strlen(message)-1]=='\n')
 				message[strlen(message)-1]='\0';
 			int count = 0;
 			for(int e = 0 ; e < strlen(message);e++){
 				if(checkAscii(message[e]) == 1){
 					finalmessage[count]=message[e];
 					count++;
 				}

 			}

 			finalmessage[count]='\0';
 			next=create(finalmessage);
 			if(words == NULL)
 				words = next;
 			if(i!=NULL)
 				i->next=next;
 			i=next;

 		}
 		(void) printf("The input has been successfully read. We are ready for clients now.\n");
 		fflush(in);
 		fclose(in);

 	}

	else{ /** We read from stdin instead */
 	(void) printf("Welcome to Hangman 2016 edition, please enter the words that you'd like to play with, and end this step with EOF:\n");
 	char message[MAX_WORD_LENGTH];
 	struct Words *next;
 	struct Words *i = NULL;
 	char finalmessage[MAX_WORD_LENGTH];
 	while(fgets(message,sizeof(message),stdin)!=NULL){

 		if(message[strlen(message)-1]=='\n')
 			message[strlen(message)-1]='\0';
 		int count = 0;
 		for(int e = 0 ; e < strlen(message);e++){
 			if(checkAscii(message[e]) == 1){
 				finalmessage[count]=message[e];
 				count++;
 			}

 		}
 		finalmessage[count]='\0';
 		next=create(finalmessage);

 		if(words == NULL)
 			words = next;
 		if(i!=NULL)
 			i->next=next;
 		i=next;

 	}

 	(void) printf("The input has been successfully read. We are ready for clients now.\n");
 }


 while(1){


 	sem_wait(s1);
		if(game->flag==TERMINATED) //checking to see whether the last client has been terminated
			clientTerminated();
		sem_post(s3);
		sem_wait(s1);
		
		if(game->flag == NEWGAME){
			++id;
			game->id=id;
			if(players == NULL)
				players = create1(id);
			else{
				struct Player *temp = NULL;
				struct Player *prev = NULL;
				temp=players;
				prev=temp;
				while(temp!=NULL){
					prev=temp;
					temp=temp->next;
				}
				prev->next=create1(id);
			}
			game->games=1;
			game->mistakes=0;
			play();
			if(game->flag==TERMINATED){
				clientTerminated();
				continue;
			}
			sem_post(s3);
			

			continue;
		}
		if(game->flag == PLAY){
			guess();
			if(game->flag==TERMINATED){
				clientTerminated();
				continue;
			}
			sem_post(s3);
			continue;
		}
		if(game->flag == REPLAY){

			play();
			sem_post(s3);
			if(game->flag==TERMINATED){
				clientTerminated();
				continue;
			}
			continue;

		}
		
	}

	
	free_resources();

	return 0;
}