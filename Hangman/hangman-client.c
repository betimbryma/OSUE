		/**
		 * @file hangman-client.c
		 * @author Betim Bryma <e1325255@student.tuwien.ac.at>
		 * @date 05.01.2016
		 * @brief This module behaves like the client that plays hangman, by interacting with the server
		 * @details A client that plays hangman with a server via the shared memory comunications
		*/

		#include <stdio.h>
		#include <unistd.h>
		#include <semaphore.h>
		#include <fcntl.h>
		#include <sys/mman.h>
		#include <sys/stat.h>
		#include <sys/types.h>
		#include <string.h>
		#include <semaphore.h>
		#include <stdlib.h>
		#include <signal.h>
		#include <ctype.h>
		#include <errno.h>
		#include "hangman.h"

		#define SEM_1 "/semaphore_1"
		#define SEM_2 "/semaphore_2"
		#define SEM_3 "/semaphore_3"
		#define SHM_NAME "/Betimemory"
		#define PERMISSION (0600)
		#define MAX_WORD_LENGTH (255)
		#define ERRORS_MAX (9)
		#define POSSIBLE_CHARACTERS (27)

		/** @brief semaphore s1 which tells the server when it can proceed*/
		 static sem_t *s1 = NULL;

		/** @brief semaphore s2 which tells the clie when it can interact with the critical area*/
		 static sem_t *s2 = NULL;

		/** @brief semaphore s3 which tells the client when it can proceed*/
		 static sem_t *s3 = NULL;

		/** @brief where the guesses for the current round are saved*/
		 static char guesses[POSSIBLE_CHARACTERS];

		/** @brief a coutner for the guesses array*/
		 static int count=0;

		/** @brief the name of the program*/
		 static const char *progname = "hangman-client";

		/** @brief inside has the state of whether the client is inside the critical area*/
		 static int inside=0;

		/** @brief the mistakes for this round*/
		 static int mistakes=0;

		/** @brief the wins so far*/
		 static int wins = 0;

		/** @brief the losts so far*/
		 static int losts = 0;

		/** @brief the total games*/
		 static int games = 0;

		/** @brief the ID of the client*/
		 static int id =0;

		 /** @brief where we are going to save the messages while communicating with the server*/
		 static char message[MAX_WORD_LENGTH];



		/**
		 * @brief frees the allocated memory and informs the server about the shutdown
		 * @details it is going to use the following global variables: s1, s2, s3, wins, losts because
		 * it needs to take care of freeing up the memory, and the wins and losts are used for displaying the final message
		*/
		 static void free_resources(void);

		/**
		 * @brief if an error occurs, then we want to shutdown this client
		 * @details it is going to use the progname variable (the name of the program)
		 * @param exitcode is the exit value of the program
		 * @param message eventual informations about the error that occured
		*/
		 static void bail_out(int exitcode, char *message);

		/**
		 * @brief the handler method for SIGINT and SIGTERM
		 * @details because the program cannot terminate without freeing the memory and taking care of semaphores
		 * @param sig the signal
		*/
		 static void terminating(int sig);

		/**
		 * @brief this method checks whether our list of guesses contains a specific character
		 * @details it is going to use the guesses list and also the counter to determine if at some point that character was allowed and used
		 * @param c the character that we want to check
		 * @return if the guesses list contains the character return 0, otherwise 1
		*/
		 static int contains(char c);


		/**
		 * @brief trying saves a specific character in our list of guesses - in a chronologicial order
		 * @details it is going to save that character in a chronological order in our guesses list, byt using the counter as well
		 * @param c the character that we want to save in our list of guesses
		*/
		 static void trying(char c);

		/**
		 * @brief printTryings is going to print out the characters that we've tried so far
		 * @details it is going to use the list of guesses defiend in the global variables
		*/
		 static void printTryings(void);


		/**
		 * @brief checkAscii is going to check whether a specific character is ASCII character or not
		 * @details in our case only letters and the 'space' is allowes, everything else is considered as false
		 * @param the character that we want to check for
		 * @return 0 if it does not fulfill the conditions, otherwise 1
		*/
		 static int checkAscii(char c);

		/**
		 * @brief the function that is going to catch our signals
		 * @param sig, represents the signal
		 * @param the second parameter represents a pointer to the handler function
		 * @return returns 0 on success; on error, -1 is returned
		*/
		 static int catch_signal(int sig, void (*handler)(int));

		/**
		 * @brief clear is simply goign to clear the list the tryings that we have done for the previous round
		 * @details it is going to use the list of guesses defined in the global variables
		*/
		 static void clear(void);

		/**
		 * @brief enumration used for 'Flags' in the process of communication between the client and the server
		*/
		 typedef enum
		 {
		 NEWGAME, /** the client onts to start a new game, as a new client*/
		 REPLAY, /** the client wants to play another game*/
		 TERMINATED, /** the client has been terminated*/
		 PLAY /** the client is playin a current game*/
		 }request;


		static struct Game *game = NULL; /** a pointer to the game struct*/

		/**
		 * @brief the game struct that is goign to be used for communication between the client and the server via shared memory
		*/
		 struct Game{
			int id; /** the ID of the client*/
			int error; /** whether something went wrong */
			char guess; /** the character that the client guessed*/
			int server_alive; /** whether the server is alive (1) or not (0)*/
			int won; /** whether the client won the game (1) or not (0)*/
			int lost; /** whether the client lost (1) or not (0)*/
			request flag; /** the flag that is used for commucation, to distinct cases*/
			int mistakes; /** how many mistakes has the client made so far*/
			int wins; /** how many times has the client won so far*/
			int losts; /** how many times has the client lost so far */
			char response[MAX_WORD_LENGTH]; /** the responses from the server */
			int games; /** how many games have been played so far*/

		 };



		 static void bail_out(int exitcode, char *message){
		 	(void) fprintf(stderr,"%s: ", progname);
		 	(void) fprintf(stderr,"%s",message);
		 	if(errno!=0)
		 		(void) fprintf(stderr,": %s",strerror(errno));
		 	(void) fprintf(stderr,"\n");
		 	free_resources();
		 	exit(exitcode);
		 }

		 static void free_resources(void){
		 	(void) printf("Here are your final results: Wins: %i Losts: %i\nHave a nice day\n",wins,losts);
		 	if(inside == 0){

		 		sem_wait(s2);
		 		game->flag=TERMINATED;
		 		game->id=id;
		 		sem_post(s2);
		 	}
		 	else{

		 		game->flag=TERMINATED;
		 		game->id=id;
		 		sem_post(s2);
		 	}

		 	if((munmap(game,sizeof *game)) == -1)
		 		(void) fprintf(stderr, "%s\n", "munmap failed");
		 	if((sem_close(s1)) == -1)
		 		(void) fprintf(stderr, "%s\n", "sem_close failed");
		 	if((sem_close(s2)) == -1)
		 		(void) fprintf(stderr, "%s\n", "sem_close failed");
		 	if((sem_close(s3)) == -1)
		 		(void) fprintf(stderr, "%s\n", "sem_close failed");

		 }

		 static void terminating(int sig){
		 	(void) printf("\nThe client is now being shut down \n");
		 	free_resources();

		 	exit(1);
		 }


		 static int contains(char c){
		 	int e = 1;
		 	for(int i=0;i<POSSIBLE_CHARACTERS;i++){
		 		if(guesses[i] == c)
		 			e = 0;
		 	}
		 	return e;
		 }

		 static void trying(char c){
		 	if(count<POSSIBLE_CHARACTERS){
		 		guesses[count]=c;
		 		count++;
		 		guesses[count]='\0';
		 	}
		 }

		 static void clear(void){
		 	for(int i=0;i<POSSIBLE_CHARACTERS;i++){
		 		guesses[i] = ' ';

		 	}
		 	count=0;
		 }

		 static void printTryings(void){
		 	(void) printf("You have tried these characters so far: ");

		 	(void) printf("%s\n ",guesses);


		 }

		 static int checkAscii(char c){
		 	if(c==32 || (c >= 65 && c <= 90) || (c >= 97 && c <= 122))
		 		return 1;
		 	else
		 		return 0;
		 }

		 static int catch_signal(int sig, void (*handler)(int)){
		 	struct sigaction action;
		 	action.sa_handler = handler;
		 	sigemptyset(&action.sa_mask);
		 	action.sa_flags = 0;
		 	return sigaction (sig, &action, NULL);
		 }


		/**
		 * @brief Program entry point.
		 * @param argc The number of command line arguments
		 * @param argv An array of the command line arguments
		 * @return The program exit code
		*/
		 int main(int argc, char **argv){

		 	count=0;
		 	if(catch_signal(SIGINT, terminating) == -1)
		 		bail_out(EXIT_FAILURE,"Couldn't map the handler for SIGINT");
		 	if(catch_signal(SIGTERM, terminating) == -1)
		 		bail_out(EXIT_FAILURE,"Couldn't map the handler for SIGTERM");

		 	if(argc!=1){
		 		(void) fprintf(stderr,"USAGE: %s <input_file>",progname);
		 		exit(EXIT_FAILURE);
		 	}


		 	int shmfd = shm_open(SHM_NAME, O_RDWR | O_CREAT, PERMISSION);

		 	if(shmfd == -1)
		 		bail_out(EXIT_FAILURE,"shm_open");

		 	if(ftruncate(shmfd, sizeof *game) == -1)
		 		bail_out(EXIT_FAILURE,"ftruncate");

		 	game=mmap(NULL,sizeof *game, PROT_READ | PROT_WRITE, MAP_SHARED, shmfd, 0);

		 	if(game->server_alive == 0){
		 		(void) fprintf(stderr,"\nThe server is not available yet, sorry. PLease try again later\n");
		 		exit(EXIT_FAILURE);
		 	}
		 	

		 	if(game == MAP_FAILED)
		 		bail_out(EXIT_FAILURE,"mmap");
		 	if((close(shmfd)) == -1)
		 		bail_out(EXIT_FAILURE,"close");

		 	s1 = sem_open(SEM_1, 0);
		 	s2 = sem_open(SEM_2, 0);
		 	s3 = sem_open(SEM_3, 0);

		 	if(s1 == SEM_FAILED || s2 == SEM_FAILED || s3 == SEM_FAILED)
		 		bail_out(EXIT_FAILURE, "sem_open");

		 	int repeated = 0;
		 	int won = 0;
		 	int lost = 0;

		 	(void) printf("\nWelcome\n");
		 	while(1){

		 		if(id == 0){
		 			sem_wait(s2);
				inside=1;/**we are inside the critical area (only this client can post the s2)*/
		 			if(game->server_alive == 0){
		 				free_resources();
		 				break;
		 			}
		 			
		 			sem_post(s1);
		 			sem_wait(s3);
		 			game->id = 0;
		 			game->mistakes = 0;
		 			game->flag = NEWGAME;
		 			sem_post(s1);
		 			sem_wait(s3);
		 			wins = 0;
		 			losts = 0;
		 			mistakes = game->mistakes;
		 			id = game->id;
		 			if(game->error == 1 || game->server_alive == 0){
		 				free_resources();
		 				break;
		 			}
		 			strncpy(message, game->response, MAX_WORD_LENGTH);
		 			sem_post(s2);
		 			inside = 0; //now we are out of the critical area
		 			(void) printf("%s",hangman[mistakes]);
		 			(void) printf("\n%s\n",message);

		 		}
		 		else{

		 			if(repeated == 0){
		 				(void) printf("\nPlease enter a character: \n");
		 				char c = toupper(fgetc(stdin));
		 				if(c == '\n')
		 					c = toupper(fgetc(stdin));
		 				if(checkAscii(c) == 0){
		 					(void) fprintf(stderr, "\nPlease enter another character\n");
		 					continue;
		 				}
		 				if(contains(c) == 0){
		 					(void) fprintf(stderr,"\nYou have already tried this character\n");
		 					continue;
		 				}
		 				trying(c);
		 				sem_wait(s2);
		 				inside = 1;
		 				if(game->server_alive == 0){
		 					free_resources();
		 					break;
		 				}

		 				sem_post(s1);
		 				sem_wait(s3);
		 				game->won=0;
		 				game->lost=0;
		 				game->id=id;
		 				game->flag = PLAY;
		 				game->guess = c;
		 				game->mistakes = mistakes;
		 				game->wins=wins;
		 				game->losts=losts;
		 				if(game->server_alive == 0){
		 					free_resources();
		 					break;
		 				}

		 				
		 				sem_post(s1);
		 				sem_wait(s3);
		 				if(game->server_alive == 0 || game->error == 1){
		 					free_resources();
		 					break;
		 				}
		 				wins = game->wins;
		 				mistakes = game->mistakes;
		 				losts = game->losts;
		 				won=game->won;
		 				lost=game->lost;
		 				strncpy(message, game->response, MAX_WORD_LENGTH);
		 				sem_post(s2);
		 				inside = 0;
		 				printTryings();
		 				(void) printf("%s",hangman[mistakes]);
		 				(void) printf("%s",message);

		 				if(lost == 1){
		 					(void) printf("\nSo far, ou have won %i times and lost %i times\n",wins,losts);
		 					(void) printf("\nSorry but you lost this round, would you like to play another game? y/n\n");
		 					char y = toupper(fgetc(stdin));
		 					if(y=='\n')
		 						y = toupper(fgetc(stdin));
		 					if(y=='Y'){
		 						printf("\nGreat!\n");
		 						repeated = 1;
		 						continue;
		 					}
		 					else{
		 						free_resources();
		 						break;
		 					}
		 				}

		 				if(won == 1)
		 				{
		 					(void) printf("\nSo far, ou have won %i times and lost %i times\n",wins,losts);
		 					(void) printf("\nCongratulations you won this round, would you like to play another game? y/n\n");
		 					char y = toupper(fgetc(stdin));
		 					if(y=='\n')
		 						y = toupper(fgetc(stdin));
		 					if(y=='Y'){
		 						(void) printf("\nGreat!\n");
		 						repeated = 1;
		 						continue;
		 					}
		 					else{
		 						free_resources();
		 						break;
		 					}
		 				}
		 				
		 			}

		 			else{
		 				clear();
		 				games++;
		 				mistakes = 0;
		 				repeated = 0;
		 				sem_wait(s2);
		 				inside = 1;
		 				if(game->server_alive == 0){
		 					free_resources();
		 					break;
		 				}
		 				sem_post(s1);
		 				sem_wait(s3);
		 				game->id = id;
		 				game->mistakes = mistakes;
		 				game->flag = REPLAY;
		 				game->games=games;
		 				sem_post(s1);
		 				sem_wait(s3);
		 				if(game->error == 1 || game->server_alive == 0){
		 					free_resources();
		 					break;
		 				}
		 				strncpy(message, game->response, MAX_WORD_LENGTH);
		 				sem_post(s2);
		 				inside = 0;
		 				printf("%s",hangman[mistakes]);
		 				printf("\n%s\n",message);

		 			}

		 		}
		 	}

		 	return 0;

		 }